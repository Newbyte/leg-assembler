use std::collections::HashMap;
use std::vec::Vec;

use MemoryType::{Data, Padding};

pub(crate) const INSTRUCTION_NUM_BITS: u8 = 6;
pub(crate) type Layout = Vec<(u8, MemoryType)>;

thread_local!(static INSTRUCTION_NAME_TABLE: HashMap<&'static str, InstructionName>
	= HashMap::from([
		("noop", InstructionName::Noop),
		("jump", InstructionName::Jump),
		("add", InstructionName::Add),
		("addi", InstructionName::Addi),
		("bif", InstructionName::Bif),
		("lw", InstructionName::Lw),
		("movhi", InstructionName::Movhi),
		("mul", InstructionName::Mul),
		("sfeq", InstructionName::Sfeq),
		("sfne", InstructionName::Sfne),
		("sfeqi", InstructionName::Sfeqi),
		("sfnei", InstructionName::Sfnei),
		("sw", InstructionName::Sw),
	]));

// Every instruction is implied to have an opcode with length 6
thread_local!(static INSTRUCTION_LAYOUT_TABLE: HashMap<InstructionName, Layout>
	= HashMap::from([
		(InstructionName::Noop, vec![(10, Padding), (16, Padding)]),
		(InstructionName::Jump, vec![(26, Data)]),
		(InstructionName::Add, vec![(5, Data), (5, Data), (5, Data), (11, Padding)]),
		(InstructionName::Addi, vec![(5, Data), (5, Data), (16, Data)]),
		(InstructionName::Bif, vec![(26, Data)]),
		(InstructionName::Lw, vec![(5, Data), (5, Data), (16, Data)]),
		(InstructionName::Movhi, vec![(5, Data), (5, Padding), (16, Data)]),
		(InstructionName::Mul, vec![(5, Data), (5, Data), (5, Data), (11, Padding)]),
		(InstructionName::Sfeq, vec![(5, Padding), (5, Data), (5, Data), (11, Padding)]),
		(InstructionName::Sfne, vec![(5, Padding), (5, Data), (5, Data), (11, Padding)]),
		(InstructionName::Sfeqi, vec![(5, Padding), (5, Data), (16, Data)]),
		(InstructionName::Sfnei, vec![(5, Padding), (5, Data), (16, Data)]),
		(InstructionName::Sw, vec![(5, Data), (5, Data), (5, Data), (11, Data)]),
	]));

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub(crate) enum InstructionName {
	Noop = 0x0,
	Jump = 0x1,
	Add = 0x2,
	Addi = 0x3,
	Bif = 0x4,
	Lw = 0x5,
	Movhi = 0x6,
	Mul = 0x7,
	Sfeq = 0x8,
	Sfne = 0x9,
	Sfeqi = 0xA,
	Sfnei = 0xB,
	Sw = 0xC,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub(crate) enum MemoryType {
	Padding,
	Data,
}

#[derive(Debug)]
pub(crate) struct Instruction {
	pub name: InstructionName,
	pub parameters: Vec<u32>,
}

pub(crate) fn instruction_str_to_instruction_name(instruction_name: &str) -> InstructionName {
	INSTRUCTION_NAME_TABLE.with(|instruction_name_table| {
		match instruction_name_table.get(instruction_name) {
			Some(instruction_name) => *instruction_name,
			None => panic!("Unknown instruction \"{}\"", instruction_name),
		}
	})
}

pub(crate) fn get_instruction_layout(instruction: InstructionName) -> Layout {
	INSTRUCTION_LAYOUT_TABLE.with(|instruction_layout_table| {
		match instruction_layout_table.get(&instruction) {
			Some(instruction) => instruction.clone(),
			None => panic!("Instruction {:?} has no layout", instruction),
		}
	})
}
