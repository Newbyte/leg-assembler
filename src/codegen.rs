use std::vec::Vec;

use crate::instruction::{
	get_instruction_layout,
	Instruction,
	InstructionName,
	INSTRUCTION_NUM_BITS,
	Layout,
	MemoryType,
};

pub(crate) fn ast_to_code_string(ast: Vec<Instruction>) -> String {
	let mut code_string = String::new();

	for instruction in ast {
		let mut instruction_string = format!("\"{}", to_opcode(instruction.name));

		let layout = get_instruction_layout(instruction.name);

		let mut parameter_count = 0;

		for (segment_count, segment) in layout.iter().enumerate() {
			if segment.1 == MemoryType::Data {
		    	let value = instruction.parameters[parameter_count];
				let size = get_parameter_size(parameter_count, layout.clone());

				let parameter_string = to_binary_parameter(value, size);
				instruction_string.push_str(parameter_string.as_ref());

				parameter_count += 1;
			} else {
				let size = get_segment_size(segment_count, layout.clone());

				let segment_string = to_binary_parameter(0, size);
				instruction_string.push_str(segment_string.as_ref());
			}
		}

		instruction_string.push_str("\",\n");

		code_string.push_str(&instruction_string);
	}

	code_string
}

fn to_opcode(instruction_name: InstructionName) -> String {
	to_binary_parameter(instruction_name as u32, INSTRUCTION_NUM_BITS)
}

fn to_binary_parameter(value: u32, size: u8) -> String {
	let code = format!("{:b}", value);

	let num_padding_bits = size as usize - code.len();
	let padding_bits = "0".repeat(num_padding_bits);

	format!("{}{}", padding_bits, code)
}

fn get_parameter_size(index: usize, layout: Layout) -> u8 {
	let mut count = 0;

	for segment in &layout {
		if segment.1 == MemoryType::Data {
			if count == index {
				return segment.0;
			}

			count += 1;
		}
	}

	panic!("Index {} is out of bounds for layout {:?}", index, layout);
}

fn get_segment_size(index: usize, layout: Layout) -> u8 {
	layout[index].0
}
