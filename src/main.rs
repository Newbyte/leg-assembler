mod codegen;
mod instruction;
mod parser;

use std::env;
use std::{fs, fs::File};
use std::io::Write;

const DEFAULT_FILENAME: &str = "a.out";

fn main() {
	let args = env::args_os();
	let filename = if args.len() == 2 {
		args.last().unwrap()
	} else {
		panic!("No input file specified");
	};

	let source_code = fs::read_to_string(filename)
		.expect("Something went wrong reading the file");
    
	let ast = parser::source_to_ast(source_code);
	let code_string = codegen::ast_to_code_string(ast);

	let mut file = File::create(DEFAULT_FILENAME).unwrap();
	writeln!(&mut file, "{}", code_string).unwrap();
}
