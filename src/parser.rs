use std::vec::Vec;

use crate::instruction::{instruction_str_to_instruction_name, Instruction};

const COMMENT_INDICATOR: &str = ";";

pub(crate) fn source_to_ast(source_code: String) -> Vec<Instruction> {
	let lines = source_code.lines();
	let mut ast = Vec::new();
	
	for line in lines {
		let mut line_split = line.split(COMMENT_INDICATOR);
		let line_without_comments = match line_split.nth(0) { // Ignore everything after the ;
			Some(line_without_comments) => line_without_comments,
			None => line,
		};

		let mut components = line_without_comments.split_whitespace();
		let mut parameters = Vec::new();
		
		let line_without_whitespace = match components.next() {
			Some(line_without_whitespace) => line_without_whitespace,
			None => continue,
		};

		if line_without_whitespace.len() == 0 {
			continue;
		};

		let name = instruction_str_to_instruction_name(line_without_whitespace);
		
		for parameter in components {
			parameters.push(parameter_to_number(parameter));
		}
		
		ast.push(Instruction { name, parameters });
	}
	
	ast
}

/// Panics on empty strings
fn parameter_to_number(parameter_str: &str) -> u32 {
	// If the parameter string starts with an r, it's a register
	if parameter_str.chars().nth(0) == Some('r') {
		// Just strip away the r since e.g. r0 is represented by 0 anyway
		parameter_to_number(&parameter_str[1..])
	} else {
		parameter_str.parse::<u32>().unwrap()
	}
}

#[test]
fn test_parameter_to_number() {
	assert_eq!(parameter_to_number("r0"), 0);
	assert_eq!(parameter_to_number("r15"), 15);
	assert_eq!(parameter_to_number("0"), 0);
	assert_eq!(parameter_to_number("15"), 15);
}
